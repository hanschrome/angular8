export class BillModel {
    public _id: string;
    public userId: number;
    public amount: string;
    public status: string;
    public payPalConfig?
}