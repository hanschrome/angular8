export class UserModel {
    id: string;
    name: string;
    email: string;
    picture: string;
    firstName: string;
    lastName: string;
}