export const appConstants = {
    accessTokenCookieName: 'accessToken',
    billStatuses: {
        paid: 'PAID',
        notPaid: 'NOT PAID'
    }
}