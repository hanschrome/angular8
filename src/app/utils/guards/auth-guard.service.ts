import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieServiceWrapper } from '../services/cookie-wrapper.service';
import { appConstants } from '../constants/app.constants';
import { PlatfromService } from 'src/app/utils/services/platform.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private _cookieService: CookieServiceWrapper, private router: Router,
        private platformService: PlatfromService) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (this.platformService.isBrowser()) {
            if (this._cookieService.get(appConstants.accessTokenCookieName)) {
                return true;
            }
            else {
                this.router.navigate(['']);
                return false;
            }
        }
        else {
            return true;
        }
    }
}