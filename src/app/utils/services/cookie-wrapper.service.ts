import { Injectable } from "@angular/core";
import { CookieService } from "ngx-cookie-service";

@Injectable()
export class CookieServiceWrapper {
    constructor(private _cookieService: CookieService) { }

    get(name: string): string {
        return this._cookieService.get(name);
    }

    set(name: string, value: string, expires?: number | Date): void {
        return this._cookieService.set(name, value, expires);
    }

    remove(name: string) {
        this._cookieService.delete(name);
    }
}