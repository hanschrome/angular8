import { Injectable, Inject, PLATFORM_ID } from "@angular/core";
import { isPlatformBrowser, isPlatformServer } from "@angular/common";

@Injectable()
export class PlatfromService {
    constructor(@Inject(PLATFORM_ID) private platformId: Object) {

    }

    isBrowser(): boolean {
        if (isPlatformBrowser(this.platformId)) {
            return true;
        }
        else {
            return false;
        }
    }

    isServer(): boolean {
        if (isPlatformServer(this.platformId)) {
            return true;
        }
        else {
            return false;
        }
    }
}