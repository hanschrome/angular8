import { Injectable } from "@angular/core";
import { CookieServiceWrapper } from "./cookie-wrapper.service";
import { appConstants } from "../constants/app.constants";
import { UserModel } from "src/app/models/user.model";

@Injectable()
export class AuthenticationService {
    public loggedInUser: UserModel;
    constructor(private _cookieService: CookieServiceWrapper) { }

    logout(): void {
        this.loggedInUser = new UserModel();
        this._cookieService.remove(appConstants.accessTokenCookieName);
    }
}