import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class HttpRestClient {
    constructor(private http: HttpClient) { }

    get(url: string, options?): Observable<any> {
        return this.http.get(url, options);
    }

    post(url: string, body: any): Observable<any> {
        return this.http.post(url, body);
    }

    put(url: string, body: any): Observable<any> {
        return this.http.put(url, body);
    }
}