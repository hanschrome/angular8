import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { HttpRestClient } from "src/app/utils/services/http-rest-client.service";
import { environment } from "src/environments/environment";
import { CookieServiceWrapper } from "../utils/services/cookie-wrapper.service";
import { UserModel } from "../models/user.model";
import { AuthenticationService } from "../utils/services/authentication.service";

@Component({
    selector: 'oAuth',
    templateUrl: 'oAuth.component.html'
})

export class OAuthComponent implements OnInit {
    constructor(private route: ActivatedRoute, private router: Router,
        private cookieService: CookieServiceWrapper, private httpRestClient: HttpRestClient,
        private authService: AuthenticationService) { }

    ngOnInit(): void {
        this.route.queryParamMap.subscribe((params: ParamMap) => {
            let code = params.get('code');
            if (code) {
                this.httpRestClient.post(environment.url + '/login', { code: code })
                    .subscribe((data: { accessToken: string, user: UserModel }) => {
                        this.authService.loggedInUser = data.user;
                        this.cookieService.set('accessToken', data.accessToken);
                        this.router.navigate(['/dashboard']);
                    })
            }
        });
    }
}