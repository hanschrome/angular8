import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes'
import { LoginComponent } from './login/login.component';
import { LoginResolver } from './login/login.resolve';
import { LoginService } from './login/login.service';
import { OAuthComponent } from './oAuth/oAuth.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardService } from './dashboard/dashboard.service';
import { DashboardResolver } from './dashboard/dashboard.resolve';
import { CookieService } from 'ngx-cookie-service';
import { HttpInterceptorService } from 'src/app/interceptor/http-interceptor.service';
import { HttpRestClient } from 'src/app/utils/services/http-rest-client.service';
import { CookieServiceWrapper } from './utils/services/cookie-wrapper.service';
import { AuthGuard } from './utils/guards/auth-guard.service';
import { AuthenticationService } from './utils/services/authentication.service';
import { NgxSpinnerModule } from "ngx-spinner";
import { LoaderService } from 'src/app/utils/services/loader.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxPayPalModule } from 'ngx-paypal';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { PlatfromService } from './utils/services/platform.service';

@NgModule({
  declarations: [
    AppComponent, LoginComponent, OAuthComponent, DashboardComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'base-project' }), HttpClientModule, RouterModule.forRoot(appRoutes), NgxSpinnerModule,
    CommonModule, FormsModule, NgxPayPalModule, TransferHttpCacheModule
  ],
  providers: [LoginResolver, LoginService,
    DashboardService, DashboardResolver, CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }, HttpRestClient, CookieServiceWrapper, AuthGuard, AuthenticationService, LoaderService, PlatfromService],
  bootstrap: [AppComponent]
})
export class AppModule { }
