import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component'
import { LoginResolver } from './login/login.resolve';
import { OAuthComponent } from './oAuth/oAuth.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardResolver } from './dashboard/dashboard.resolve';
import { AuthGuard } from './utils/guards/auth-guard.service';

export const appRoutes: Routes = [
    { path: '', component: LoginComponent, resolve: { url: LoginResolver } },
    { path: 'oauth', component: OAuthComponent, },
    { path: 'dashboard', component: DashboardComponent, resolve: { user: DashboardResolver }, canActivate: [AuthGuard] }
]