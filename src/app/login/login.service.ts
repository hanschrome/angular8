import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpRestClient } from "../utils/services/http-rest-client.service";
import { environment } from "src/environments/environment.prod";

@Injectable()
export class LoginService {
    constructor(private http: HttpRestClient) { }

    getGoogleOAuthUrl(): Observable<any> {
        return this.http.get(environment.url + '/url', { responseType: 'text' });
    }
}