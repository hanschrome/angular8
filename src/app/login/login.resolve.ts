import { Injectable } from "@angular/core";
import {
    Router, Resolve,
    RouterStateSnapshot,
    ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, throwError } from "rxjs";
import { map, catchError } from 'rxjs/operators'
import { LoginService } from "src/app/login/login.service";
import { HttpErrorResponse } from "@angular/common/http";
import { CookieServiceWrapper } from "../utils/services/cookie-wrapper.service";
import { appConstants } from "../utils/constants/app.constants";

@Injectable()
export class LoginResolver implements Resolve<any> {
    constructor(private loginService: LoginService, private router: Router, private cookieService: CookieServiceWrapper) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        if (this.cookieService.get(appConstants.accessTokenCookieName)) {
            this.router.navigate(['/dashboard']);
        }
        else {
            return this.loginService.getGoogleOAuthUrl()
                .pipe(
                    map(url => {
                        if (url) {
                            return url;
                        } else { // url not found
                            return throwError('url not found');
                        }
                    }),
                    catchError((error: HttpErrorResponse) => {
                        let errorMessage = '';
                        if (error.error instanceof ErrorEvent) {
                            // client-side error
                            errorMessage = `Error: ${error.error.message}`;
                        } else {
                            // server-side error
                            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
                        }
                        return throwError(errorMessage);
                    })
                )
        }
    }
}