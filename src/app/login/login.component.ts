import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: 'login',
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    public googleOAuthUrl: string;
    public user; loggedIn;
    constructor(private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.route.data
            .subscribe((data: { url: string }) => {
                this.googleOAuthUrl = data.url;
            });
    }
}