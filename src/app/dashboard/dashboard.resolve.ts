import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Observable, throwError, of } from "rxjs";
import { DashboardService } from "../dashboard/dashboard.service";
import { UserModel } from "../models/user.model";
import { map, catchError } from "rxjs/operators";
import { AuthenticationService } from "../utils/services/authentication.service";
import { HttpErrorResponse } from "@angular/common/http";

@Injectable()
export class DashboardResolver implements Resolve<any> {

    constructor(private dashboardService: DashboardService, private router: Router,
        private authService: AuthenticationService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        if (this.authService.loggedInUser && this.authService.loggedInUser.id) {
            return of(this.authService.loggedInUser);
        }
        else {
            return this.dashboardService.getUserData()
                .pipe(
                    map((user: UserModel) => {
                        if (user.id && user.email) {
                            return user;
                        } else { // id not found
                            this.authService.logout();
                            this.router.navigate(['']);
                            return false;
                        }
                    }),
                    catchError((error: HttpErrorResponse) => {
                        let errorMessage = '';
                        if (error.error instanceof ErrorEvent) {
                            // client-side error
                            errorMessage = `Error: ${error.error.message}`;
                        } else {
                            // server-side error
                            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
                        }
                        alert(errorMessage);
                        this.authService.logout();
                        this.router.navigate(['']);
                        return throwError(errorMessage);
                    })
                )
        }

    }
}