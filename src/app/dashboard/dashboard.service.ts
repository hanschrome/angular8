import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpRestClient } from "../utils/services/http-rest-client.service";
import { environment } from "src/environments/environment";

@Injectable()
export class DashboardService {
    constructor(private http: HttpRestClient) { }

    getUserData(): Observable<any> {
        return this.http.get(environment.url + '/user');
    }

    saveUserData(userData): Observable<any> {
        return this.http.post(environment.url + '/user', userData);
    }

    getUserBills(): Observable<any> {
        return this.http.get(environment.url + '/bills');
    }

    saveBillData(billId: number, body): Observable<any> {
        return this.http.put(environment.url + '/bill?id=' + billId, body);
    }

    editImage(file, imageName): Observable<any> {
        let formData: FormData = new FormData();
        formData.append('imageFile', file, imageName);
        return this.http.post(environment.url + '/file_upload', formData);

    }
}