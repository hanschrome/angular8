import { Component, OnInit, DoCheck, ChangeDetectorRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UserModel } from "../models/user.model";
import { NgForm } from "@angular/forms";
import { DashboardService } from "./dashboard.service";
import { AuthenticationService } from "../utils/services/authentication.service";
import { BillModel } from "../models/bill.model";
import { ICreateOrderRequest } from "ngx-paypal";
import { appConstants } from "../utils/constants/app.constants";
import { environment } from "./../../environments/environment";

@Component({
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit {
    public userModel: UserModel;
    public userBills: Array<BillModel> = [];
    public appConst = appConstants;
    public env = environment;
    constructor(private route: ActivatedRoute,
        private dashboardService: DashboardService,
        private _authService: AuthenticationService,
        private router: Router,
        private cdRef: ChangeDetectorRef) { }

    private initConfig(price: string, billId: string): any {
        return {
            currency: 'USD',
            createOrderOnClient: (data) => <ICreateOrderRequest>{
                intent: 'CAPTURE',
                purchase_units: [{
                    amount: {
                        currency_code: 'USD',
                        value: price,
                    },
                    custom_id: billId
                }]
            },
            onClientAuthorization: (data) => {
                console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
                let callBackData = data.purchase_units[0];
                this.setBillStatus(callBackData.custom_id, this.appConst.billStatuses.paid);
            },
            onError: err => {
                console.log('OnError', err);
            },
        };
    }

    ngOnInit(): void {
        this.route.data.subscribe((data: { user: UserModel }) => {
            this.userModel = data.user;
            this.userModel.picture = environment.url + '/' + this.userModel.picture;
            this._authService.loggedInUser = this.userModel;
        });
        this.getUserBills();
    }

    getUserBills(): void {
        this.userBills = [];
        this.dashboardService.getUserBills()
            .subscribe((bills: Array<BillModel>) => {
                bills.forEach((bill, index) => {
                    if (bill.status != this.appConst.billStatuses.paid) {
                        bill.payPalConfig = this.initConfig(bill.amount, bill._id);
                    }
                });
                this.userBills = bills;
                this.cdRef.detectChanges();
            });
    }

    onSubmit(userForm: NgForm): void {
        if (userForm.valid) {
            let body = {
                firstName: this.userModel.firstName,
                lastName: this.userModel.lastName
            }
            this.dashboardService.saveUserData(body)
                .subscribe((resp) => {
                    alert('user updated successfully');
                }, (err) => {
                    alert('user not updated.')
                })
        }
    }

    setBillStatus(billId, status): void {
        // let body = {
        //     status: status
        // }
        // this.dashboardService.saveBillData(billId, body)
        //     .subscribe(bill => {
        //         this.getUserBills();
        //         alert('Bill updated successfully');
        //     },
        //         err => {
        //             alert('Bill not updated');
        //         })
        alert('Bill updated successfully');
        this.userBills.find((bill) => bill._id === billId).status = this.appConst.billStatuses.paid;
        this.cdRef.detectChanges();
    }

    onFileSelect(file: Array<File>) {
        let img = file[0];
        this.dashboardService.editImage(file[0], this.userModel.id + '.' + img.name.split('.')[1])
            .subscribe(res => {
                var reader = new FileReader();
                reader.onload = (e) => {
                    //we are showing the image from user system only when the user changes the image
                    this.userModel.picture = <any>e.target['result'];
                };
                // read the image file as a data URL.
                reader.readAsDataURL(file[0]);
            },
                err => {
                    alert('image not uploaded');
                    console.log(err);
                })
    }


    logout(): void {
        this._authService.logout();
        this.router.navigate(['']);
    }
}