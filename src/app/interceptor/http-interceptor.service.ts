import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { CookieServiceWrapper } from '../utils/services/cookie-wrapper.service';
import { LoaderService } from '../utils/services/loader.service';
import { appConstants } from '../utils/constants/app.constants';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
    public counterObj = {
        count: 0
    }
    constructor(private _cookieService: CookieServiceWrapper,
        private _loaderService: LoaderService) {

    }


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.counterObj.count++;
        this._loaderService.showLoader();
        if (this._cookieService.get(appConstants.accessTokenCookieName)) {
            req = req.clone({ headers: req.headers.append('Authorization', 'Bearer ' + this._cookieService.get("accessToken")) });
        }
        return next.handle(req).pipe(
            tap(event => {
                if (event instanceof HttpResponse) {
                    this.onResponse(event);
                }
            },
                err => this.responseError(err)
            ));
    }
    onResponse(resp) {
        this.hideLoader();
        return resp;
    }

    responseError(rejection) {
        this.hideLoader();
        return observableThrowError(rejection);
    }

    hideLoader(): void {
        this.counterObj.count--;
        (this.counterObj.count === 0 || this.counterObj.count < 0) ?
            this._loaderService.hideLoader() : this._loaderService.showLoader();
    }
}
